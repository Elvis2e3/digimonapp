import React from 'react';
import logo from './logo.svg';
import './App.css';

function DigiImg({digimonView}){
    return (
        <section className="DigiImg">
            <h2>{ digimonView.name}</h2>
            <img src={digimonView.img} width='60%' />
        </section>
    );
}

export { DigiImg };
