import React from 'react';
import './App.css';

function DigiList(props){
    return (
        <section className="DigiList">
            <ul>
                {props.children}
            </ul>
        </section>
    );
}

export { DigiList };
