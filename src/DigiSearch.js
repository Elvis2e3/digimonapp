import React from 'react';
import './App.css';

function DigiSearch({searchValue, setSearchValue}){

    const onSearchValueChange = (event) => {
        setSearchValue(event.target.value)
    };

    return (
        <input
            className='DigiSearch'
            type="search"
            placeholder="Search Digimons"
            value={searchValue}
            onChange={onSearchValueChange}
        />
    );
}

export { DigiSearch };
