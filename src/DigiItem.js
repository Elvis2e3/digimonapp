import React from 'react'
import './App.css';

function DigiItem(props){
    const onClickItem = (digimon) => {
        props.setDigimonView(digimon)
    }
    return (
        <li className="DigiItem" onClick={() => onClickItem(props.digimon)}>
            <div className='DigiCount'>
                <div className="DigiItem-p-img">
                    <img src={props.digimon.img} width='80px' />
                </div>
                <div className="DigiItem-p">
                    <p ><strong>Name: </strong> {props.digimon.name}</p>
                    <p ><strong>Level: </strong> {props.digimon.level}</p>
                </div>
            </div>
        </li>
    );
}

export { DigiItem };
